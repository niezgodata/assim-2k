var path = "images/";
var min_pic = 1;
var max_pic = 2006;



var CMIP5_mean = document.getElementById("CMIP5_mean");
//var CMIP5_std = document.getElementById("CMIP5_std");
var CMIP6_mean = document.getElementById("CMIP6_mean");
//var CMIP6_std = document.getElementById("CMIP6_std");


var stk = {
	"CMIP5mean" : [],
//	"CMIP5std" : [],
	"CMIP6mean" : [],
//	"CMIP6std" : []
};

var cnt = {
	"CMIP5mean": 0,
//	"CMIP5std": 0,
	"CMIP6mean": 0,
//	"CMIP6std": 0
};


var ext = '.webp';

for (var k=min_pic; k <= max_pic; k++) {
 
	var img;

	img = document.createElement("img");
	img.setAttribute('data-src', path + 'CMIP5/' + 'Mean_Rec_year_' + k + ext);
	img.onload = function() { loadNextCMIP5mean() };
	stk["CMIP5mean"].push(img);
	CMIP5_mean.appendChild(img);

//	img = document.createElement("img");
//	img.setAttribute('data-src', path + 'CMIP5/' + 'Ens_std_year_' + k + ext);
//	img.onload = function() { loadNextCMIP5std() };
//	stk["CMIP5std"].push(img);
//	CMIP5_std.appendChild(img);

	img = document.createElement("img");
	img.setAttribute('data-src', path + 'CMIP6/' + 'Mean_Rec_year_' + k + ext);
	img.onload = function() { loadNextCMIP6mean() };
	stk["CMIP6mean"].push(img);
	CMIP6_mean.appendChild(img);
 
//	img = document.createElement("img");
//	img.setAttribute('data-src', path + 'CMIP6/' + 'Ens_std_year_' + k + ext);
//	img.onload = function() { loadNextCMIP6std() };
//	stk["CMIP6std"].push(img);
//	CMIP6_std.appendChild(img);

} 


var pics_slider = document.getElementById("pics_slider");
var pics_steplist = document.getElementById("pics_steplist");

pics_slider.setAttribute("max", min_pic);

var slider_min = document.getElementById("slider_min");
var slider_max = document.getElementById("slider_max");

var slider_content = document.getElementById("slider_content");

slider_min.innerHTML = min_pic;

var lastdone = 0;

function loadNext(k) {
	
	if (cnt[k] >= stk[k].length) {
		return
	}

	console.log('Loading ' + k + ' pic #' + cnt[k]);
	stk[k][cnt[k]].src = stk[k][cnt[k]].getAttribute("data-src");
	cnt[k]++;
		
	var max_c = max_pic;
	for (var c in cnt) {
		max_c = Math.min(max_c, cnt[c]);
	}

	slider_content.style.width = (max_c / max_pic * 96) + '%';

	pics_slider.setAttribute("max", max_c);
	slider_max.innerHTML = max_c;

	//pics_steplist.innerHTML = "";

	if ((max_c != lastdone) && (!(max_c % 200) || (max_c == 1)) ) {

		var opt = document.createElement("option");
		opt.innerHTML = max_c;
		opt.value = max_c;
		
		if (max_c < 10){
			opt.label = '000'+max_c;
		} else if (max_c < 100){
			opt.label = '00'+max_c;
		} else if (max_c < 1000){
			opt.label = '0'+max_c;
		} else {
			opt.label = max_c;
		}
			
		pics_steplist.appendChild(opt);

		lastdone = max_c;
	}

	pics_steplist.style.padding = '0 calc(' +( (max_c % 200) / (max_c + 1) * 100 )+ '%) 0 0';
}

function loadNextCMIP5mean () {
	loadNext("CMIP5mean");
}

//function loadNextCMIP5std () {
//	loadNext("CMIP5std");
//}

function loadNextCMIP6mean () {
	loadNext("CMIP6mean");
}

//function loadNextCMIP6std () {
//	loadNext("CMIP6std");
//}




pics_slider.addEventListener("change", function () {

	var n = this.value;

	for (var ox in stk) {
		for (var i of stk[ox]) {
			i.classList.remove("active");
		}

		stk[ox][n-1].classList.add("active");
	}

	

});



loadNextCMIP5mean();
//loadNextCMIP5std();
loadNextCMIP6mean();
//loadNextCMIP6std();

stk["CMIP5mean"][0].classList.add("active");
//stk["CMIP5std"][0].classList.add("active");
stk["CMIP6mean"][0].classList.add("active");
//stk["CMIP6std"][0].classList.add("active");



